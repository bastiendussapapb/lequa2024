#!/bin/bash

source ./.env_lequa2024/bin/activate

# Create variable method
method="RFFMScaler"

python3 ./script/T1.py --method $method
python3 ./script/format_checker.py --method $method --task T1
python3 ./script/evaluate.py --method $method --task T1
