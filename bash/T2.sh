#!/bin/bash

source ./.env_lequa2024/bin/activate

# Create variable method
method="MahalanobisFourierClassifier"

python3 ./script/T2.py --method $method
python3 ./script/format_checker.py --method $method --task T2
python3 ./script/evaluate.py --method $method --task T2
