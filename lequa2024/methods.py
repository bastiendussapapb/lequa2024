from quapy.data import LabelledCollection
from typing import Generator
from tqdm import tqdm
from sklearn.linear_model import LogisticRegression
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import torch
import numpy as np

from .data import ResultSubmission
from .constants import DEV_SAMPLES
from .mahalanobis import (
    RFF_mahalanobis,
    DFM_mahalanobis,
    M_identity,
    M_estimation,
    RFFM_mahalanobis
)

from quapy.method.aggregative import CC, ACC, PACC, EMQ
from quapy.method.non_aggregative import RFFM, RFF, DFM, KernelQuantifier

####### Dummy method #######


def dummy_method(Source: LabelledCollection,
                 Target_generator: Generator) -> ResultSubmission:

    results = ResultSubmission()

    for Target_index, Target in tqdm(Target_generator, total=DEV_SAMPLES):
        results.add(Target_index, Source.p)

    return results


def ClassifyAndCount(Source: LabelledCollection,
                     Target_generator: Generator,
                     seed: float) -> ResultSubmission:

    results = ResultSubmission()

    # fit the model
    Source_train, Source_val = Source.split_random(
        train_prop=0.8, random_state=seed
    )
    clf = LogisticRegression(max_iter=1000)
    clf.fit(Source_train.instances, Source_train.labels)
    cc = CC(clf)
    cc.fit(Source_val, fit_classifier=False)

    for Target_index, Target in tqdm(Target_generator, total=DEV_SAMPLES):
        estimation = cc.quantify(Target)
        results.add(Target_index, estimation)

    return results


def BlackBoxShiftEstimator(Source: LabelledCollection,
                           Target_generator: Generator,
                           seed: float) -> ResultSubmission:

    results = ResultSubmission()

    # class to wrap the classifier
    class _BBSEVectorisation:
        def __init__(self, clf) -> None:
            self.classifier = clf

        def __call__(self, distribution):
            proba = (
                torch.from_numpy(self.classifier.predict_proba(distribution))
                .mean(dim=0)
                .to(torch.float32)
            )
            return proba / sum(proba)

    # fit the model
    Source_train, Source_val = Source.split_random(
        train_prop=0.8, random_state=seed
    )
    clf = LogisticRegression(max_iter=1000)
    clf.fit(Source_train.instances, Source_train.labels)

    bbse = DFM(vectorization=_BBSEVectorisation(clf))
    bbse.fit(Source_val)

    for Target_index, Target in tqdm(Target_generator, total=DEV_SAMPLES):
        estimation = bbse.quantify(Target, soft=False)
        results.add(Target_index, estimation)

    return results


def MaximumLikelihoodShiftEstimator(Source: LabelledCollection,
                                    Target_generator: Generator,
                                    seed: float) -> ResultSubmission:

    results = ResultSubmission()

    # fit the model
    Source_train, Source_val = Source.split_random(
        train_prop=0.8, random_state=seed
    )
    clf = LogisticRegression(max_iter=1000)
    clf.fit(Source_train.instances, Source_train.labels)

    emq = EMQ(
        classifier=clf,
        exact_train_prev=True,
        recalib=None,
    )
    emq.fit(Source, fit_classifier=False)

    for Target_index, Target in tqdm(Target_generator, total=DEV_SAMPLES):
        estimation = emq.quantify(Target)
        results.add(Target_index, estimation)

    return results


def MeanDFM(Source: LabelledCollection,
            Target_generator: Generator,
            seed: float) -> ResultSubmission:

    results = ResultSubmission()

    # Mean vectorization
    class _MeanVectorization:
        def __init__(self, *args, **kwargs):
            pass

        def __call__(self, distribution, *args, **kwargs):
            if isinstance(distribution, torch.Tensor):
                return torch.mean(distribution, dim=0)
            if isinstance(distribution, np.ndarray):
                return torch.from_numpy(np.mean(distribution, axis=0))
            else:
                raise TypeError(
                    "distribution must be a numpy array or a torch tensor not {}".format(
                        type(distribution)
                    )
                )

    vectorization = _MeanVectorization()
    deepdfm = DFM(
        vectorization=vectorization,
        seed=seed,
    )
    deepdfm.fit(Source)

    for Target_index, Target in tqdm(Target_generator, total=DEV_SAMPLES):
        estimation = deepdfm.quantify(Target, soft=False)
        results.add(Target_index, estimation)

    return results


def FourierClassifier(Source: LabelledCollection,
                      Target_generator: Generator,
                      seed: float) -> ResultSubmission:

    results = ResultSubmission()

    # Vectorisation class

    class _FourierClassifierVectorisation:
        def __init__(self, n_classes, parameters) -> None:
            self.classifier = parameters["Classifier"]
            self.RFF = RFF(
                dim=n_classes,
                sigma=parameters["sigma"],
                n_features=parameters["n_features"],
                seed=parameters["seed"],
                device=parameters["device"],
            )

        def __call__(self, distribution):
            return self.RFF(self.classifier.predict_proba(distribution))

    # fit the model
    Source_train, Source_val = Source.split_random(
        train_prop=0.7, random_state=seed
    )
    clf = LogisticRegression(max_iter=1000)
    clf.fit(Source_train.instances, Source_train.labels)

    parameters = {
        "Classifier": clf,
        "sigma": 0.3,
        "n_features": 1000,
        "seed": seed,
        "device": torch.device("cuda" if torch.cuda.is_available() else "cpu"),
    }
    vectorization = _FourierClassifierVectorisation(
        Source.n_classes, parameters)
    FourierClassifer = DFM(vectorization=vectorization)
    FourierClassifer.fit(Source_val)

    for Target_index, Target in tqdm(Target_generator, total=DEV_SAMPLES):
        estimation = FourierClassifer.quantify(Target, soft=False)
        results.add(Target_index, estimation)

    return results


def MahalanobisFourierClassifier(Source: LabelledCollection,
                                 Target_generator: Generator,
                                 seed: float) -> ResultSubmission:

    results = ResultSubmission()

    # Vectorisation class
    class _MahalanobisFourierClassifierVectorisation:
        def __init__(self, n_classes, parameters) -> None:
            self.classifier = parameters["Classifier"]
            self.RFF = RFF_mahalanobis(
                dim=n_classes,
                sigma=parameters["sigma"],
                n_features=parameters["n_features"],
                seed=parameters["seed"],
                device=parameters["device"],
            )

        def __call__(self, distribution):
            return self.RFF(self.classifier.predict_proba(distribution))
    
    class _FourierClassifierVectorisation:
        def __init__(self, n_classes, parameters) -> None:
            self.classifier = parameters["Classifier"]
            self.RFF = RFF(
                dim=n_classes,
                sigma=parameters["sigma"],
                n_features=parameters["n_features"],
                seed=parameters["seed"],
                device=parameters["device"],
            )

        def __call__(self, distribution):
            return self.RFF(self.classifier.predict_proba(distribution))
        
        
    # fit the model
    Source_train, Source_val = Source.split_random(
        train_prop=0.7, random_state=seed
    )
    clf = LogisticRegression(max_iter=1000)
    clf.fit(Source_train.instances, Source_train.labels)

    parameters = {
        "Classifier": clf,
        "sigma": 0.3,
        "n_features": 1000,
        "seed": seed,
        "device": torch.device("cuda" if torch.cuda.is_available() else "cpu"),
    }
    vectorization = _MahalanobisFourierClassifierVectorisation(
        Source.n_classes, parameters)
    MFourierClassifer = DFM_mahalanobis(
        vectorization=vectorization,
        lmbda=0.01,
        seed=seed)
    vectorizationFC = _FourierClassifierVectorisation(
        Source.n_classes, parameters)
    FourierClassifer = DFM(vectorization=vectorizationFC)
    FourierClassifer.fit(Source_val)

    for Target_index, Target in tqdm(Target_generator, total=DEV_SAMPLES):
        p1 = FourierClassifer.quantify(Target, soft=False)
        MFourierClassifer.fit(Source, sigma=parameters["sigma"],
                              mahalanobis_function=M_estimation, prop=p1)
        
        estimation = MFourierClassifer.quantify(Target, soft=False)
        results.add(Target_index, estimation)

    return results


def RandomFourierFeatureMatching(Source: LabelledCollection,
                                 Target_generator: Generator,
                                 seed: float,
                                 logger) -> ResultSubmission:

    results = ResultSubmission()
    assert torch.cuda.is_available(), "CUDA is not available"

    rffm = RFFM(
        n_features=1000,
        seed=seed,
        device=torch.device("cuda"),
    )
    rffm.fit(Source, sigma=np.linspace(0.1, 5, 20))
    logger.info(f"Best sigma: {rffm.sigma}")

    for Target_index, Target in tqdm(Target_generator, total=DEV_SAMPLES):
        estimation = rffm.quantify(Target, soft=False)
        results.add(Target_index, estimation)

    return results

def RandomFourierFeatureMatchingScaler(Source: LabelledCollection,
                                    Target_generator: Generator,
                                    seed: float,
                                    logger) -> ResultSubmission:

    results = ResultSubmission()
    assert torch.cuda.is_available(), "CUDA is not available"

    rffm = RFFM(
        n_features=1000,
        seed=seed,
        device=torch.device("cuda"),
    )
    scaler = MinMaxScaler()
    Source.instances = scaler.fit_transform(Source.instances)
    rffm.fit(Source, sigma=np.linspace(0.5, 3, 20))
    
    logger.info(f"Best sigma: {rffm.sigma}")

    for Target_index, Target in tqdm(Target_generator, total=DEV_SAMPLES):
        estimation = rffm.quantify(scaler.transform(Target), soft=False)
        results.add(Target_index, estimation)

    return results

def RandomFourierFeatureMatchingPCA(Source: LabelledCollection,
                                 Target_generator: Generator,
                                 seed: float,
                                 logger) -> ResultSubmission:

    results = ResultSubmission()
    assert torch.cuda.is_available(), "CUDA is not available"

    pca = PCA(n_components=40)
    logger.info(f"n_components: {pca.n_components}")

    rffm = RFFM(
        n_features=1000,
        seed=seed,
        device=torch.device("cuda"),
    )
    Source.instances = pca.fit_transform(Source.instances)
    
    rffm.fit(Source, sigma=np.linspace(0.1, 5, 20))
    logger.info(f"Best sigma: {rffm.sigma}")

    for Target_index, Target in tqdm(Target_generator, total=DEV_SAMPLES):
        estimation = rffm.quantify(pca.transform(Target), soft=False)
        results.add(Target_index, estimation)

    return results


def MahalanobisRandomFourierFeatureMatching(Source: LabelledCollection,
                                            Target_generator: Generator,
                                            seed: float,
                                            logger) -> ResultSubmission:

    results = ResultSubmission()
    assert torch.cuda.is_available(), "CUDA is not available"

    # no Manhalanobis
    rffm = RFFM(
        n_features=1000,
        seed=seed,
        device=torch.device("cuda"),
    )
    rffm.fit(Source, sigma=np.linspace(0.1, 5, 20))
    logger.info(f"Best sigma: {rffm.sigma}")

    Mrffm = RFFM_mahalanobis(
        n_features=1000,
        seed=seed,
        lmbda=0.01,
        device=torch.device("cuda"),
    )

    logger.info(f"Best sigma: {rffm.sigma}")

    for Target_index, Target in tqdm(Target_generator, total=DEV_SAMPLES):
        p1 = rffm.quantify(Target, soft=False)
        Mrffm.fit(Source, sigma=rffm.sigma,
                  mahalanobis_function=M_estimation, prop=p1)

        estimation = Mrffm.quantify(Target, soft=False)
        results.add(Target_index, estimation)

    return results

def MahalanobisRandomFourierFeatureMatchingScaler(Source: LabelledCollection,
                                            Target_generator: Generator,
                                            seed: float,
                                            logger) -> ResultSubmission:

    results = ResultSubmission()
    assert torch.cuda.is_available(), "CUDA is not available"

    scaler = StandardScaler()
    # no Manhalanobis
    rffm = RFFM(
        n_features=1000,
        seed=seed,
        device=torch.device("cuda"),
    )
    Source.instances = scaler.fit_transform(Source.instances)
    rffm.fit(Source, sigma=np.linspace(0.1, 5, 20))
    logger.info(f"Best sigma: {rffm.sigma}")

    Mrffm = RFFM_mahalanobis(
        n_features=1000,
        seed=seed,
        lmbda=0.01,
        device=torch.device("cuda"),
    )

    logger.info(f"Best sigma: {rffm.sigma}")

    for Target_index, Target in tqdm(Target_generator, total=DEV_SAMPLES):
        p1 = rffm.quantify(scaler.transform(Target), soft=False)
        Mrffm.fit(Source, sigma=rffm.sigma,
                  mahalanobis_function=M_estimation, prop=p1)

        estimation = Mrffm.quantify(scaler.transform(Target), soft=False)
        results.add(Target_index, estimation)

    return results
