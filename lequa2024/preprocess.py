import quapy as qp
import torch

from .data import load_vector_documents


def create_LabelledCollection_from_documents(path):
    """
    Create a LabelledCollection object from documents.

    Parameters:
        path (str): The path to the documents.

    Returns:
        qp.data.LabelledCollection: The created LabelledCollection object.
    """
    return qp.data.LabelledCollection(*load_vector_documents(path))
