import numpy as np
import torch

from cvxopt import matrix, solvers

from quapy.method.non_aggregative import (
    BaseDistributionFeatureMatching,
)
from quapy.data import LabelledCollection


class DFM_mahalanobis(BaseDistributionFeatureMatching):
    """
    :param vectorization: a `callable` use to vectorize the distribution
    :param seed: seed
    """

    def __init__(
        self,
        vectorization: callable,
        lmbda: float = 0.01,
        seed: int = 123,
    ) -> None:
        self._vectorization = vectorization
        self.seed = seed
        self.lmbda = lmbda

    def fit(
        self, data: LabelledCollection, mahalanobis_function: callable, *arg, **kwargs
    ):
        """
        Vectorize the training set.

        :param data: a :class:`quapy.data.base.LabelledCollection` consisting of the training data
        :param mahalanobis_function: a :callable:
        :return: self
        """
        sourceDis = [data.instances[data.labels == i] for i in data.classes_]
        mappingsource = [self._vectorization(source) for source in sourceDis]
        self.sourceEmb = torch.stack(
            [torch.mean(x, dim=0) for x in mappingsource], dim=0
        )

        self.M = mahalanobis_function(
            mappingsource, self.sourceEmb, self.lmbda, *arg, **kwargs
        )
        self.MM = self.M.T @ self.M

        self.GramMatrix = self.sourceEmb @ self.MM @ self.sourceEmb.T

    def quantify(self, instances, soft: bool):
        """
        Generate class prevalence estimates for the sample's instances using DFM.

        :param instances: array-like
        :return: `np.ndarray` of shape `(self.n_classes_,)` with class prevalence estimates.
        """
        mappingtarget = self._vectorization(instances)
        targetEmb = torch.mean(mappingtarget, dim=0).to(dtype=torch.float32)
        
        return self.solveQP(
            self.GramMatrix, self.sourceEmb @ self.MM @ targetEmb, soft=soft
        )

    @classmethod
    def solveQP(
        dfm, GramMatrix: torch.tensor, scalarSourceTarget: torch.tensor, soft: bool
    ):
        """
        Minimise the QP probel :math:`x^TPx - 2Qx` with :math:`P` = `GramMatrix` and :math:`Q` = `scalarSourceTarget`

        :param sourceEmb: a `np.ndarray` of shape `(n_classes,n_features)` with entry `i` being
            :math:`\Phi(\hat{P}_i)`, that is, the embedding of class `i`.
        :param targetEmb: a `np.ndarray` of shape `(n_features,)` being :math:`\Phi(\hat{Q})`, that is, the target embedding.
        :return: an `np.ndarray` of shape `(n_classes,)`.
        """

        n = GramMatrix.shape[0]  # number of classes
        if not soft:
            P = matrix(2 * GramMatrix.numpy().astype(np.double))
            q = matrix(-2 * scalarSourceTarget.numpy().astype(np.double))
            # constraint
            G = matrix(-np.eye(n))
            h = matrix(np.zeros(n))
            A = matrix(np.ones(n)).T
            b = matrix([1.0])

            solvers.options["show_progress"] = False
            sol = solvers.qp(P, q, G, h, A, b)

            return np.array(sol["x"]).reshape(n)
        else:

            def _matrix_P(tensor):
                P = tensor.cpu().numpy().astype(np.double)
                # Add 0, last column and last row
                P_bar = np.pad(P, pad_width=(0, 1))
                return matrix(P_bar.astype(np.double))

            def _matrix_q(tensor):
                q = tensor.cpu().numpy()
                q_bar = np.pad(q, pad_width=(0, 1))  # Add 0, last index
                return matrix(q_bar.astype(np.double))

            P = _matrix_P(2 * GramMatrix)
            q = _matrix_q(-2 * scalarSourceTarget)

            # constraint
            G = matrix(-np.eye(n + 1))
            h = matrix(np.zeros(n + 1))
            A = matrix(np.ones(n + 1)).T
            b = matrix([1.0])

            solvers.options["show_progress"] = False
            sol = solvers.qp(P, q, G, h, A, b)

            return np.array(sol["x"]).reshape(n + 1)[:-1]


class RFFM_mahalanobis(BaseDistributionFeatureMatching):
    def __init__(
        self,
        n_features: int = 1000,
        lmbda: float = 0.01,
        seed: int = 123,
        device: torch.device = torch.device("cpu"),
    ) -> None:
        self.seed = seed
        self.n_features = n_features
        self.lmbda = lmbda
        self.device = device

    def fit(
        self,
        data: LabelledCollection,
        mahalanobis_function: callable,
        sigma: float,
        prop: np.ndarray = None,
        *args,
        **kwargs
    ):
        """
        Compute the embedding of each class of the source.

        :param data: a :class:`quapy.data.base.LabelledCollection` consisting of the training data
        :param sigma: Bandwith hyperparameter. If it's a list, search in the list the value of sigma that
            maximise the :math \Delta_{\text{min}}: criterion.
        """
        self.dim = data.instances.shape[1]  # dimension of the data
        # sigma

        self.sigma = sigma
        self._vectorization = RFF_mahalanobis(
            self.dim, self.sigma, self.n_features, self.seed, device=self.device
        )
        self._dfm = DFM_mahalanobis(
            vectorization=self._vectorization, lmbda=self.lmbda, seed=self.seed
        )
        self._dfm.fit(data, mahalanobis_function, prop, *args, **kwargs)

    def quantify(self, instances, soft: bool):
        return self._dfm.quantify(instances, soft=soft)


class RFF_mahalanobis:
    """
    Class to compute the Random Fourier Feature vectorization of a Distribution
    """

    def __init__(
        self, dim: int, sigma: float, n_features: float, seed: int, device: torch.device
    ) -> None:
        """
        :param dim: Dimension of the data
        :param sigma: Bandwidth of the Gaussian kernel
        :param n_features: Dimension of the embedding
        :param seed: seed

        :return: self
        """
        self.seed = seed
        self.sigma = sigma
        self.n_features = n_features
        self.dim = dim
        self.device = device

        rng = np.random.default_rng(seed=self.seed)
        self.w = (
            torch.from_numpy(
                rng.normal(
                    loc=0,
                    scale=1.0 / self.sigma,
                    size=(int(self.n_features / 2), self.dim),
                )
            )
            .double()
            .to(self.device)
        )

    def __call__(self, instances: np.array):
        """
        Vectorize the `instances`

        :param instances: a `np.array`

        :return: a `np.array` of shape `(n, n_features,)`
        """
        Xw = torch.from_numpy(instances).double().to(self.device) @ self.w.T
        # Xw = Xw.cpu()
        C = torch.cat((torch.cos(Xw), torch.sin(Xw)), dim=1).to(
            device="cpu", dtype=torch.float32
        )

        return np.sqrt(2 / self.n_features) * C


#### Cov matrix ####


def _power_matrix(M: torch.tensor, exponent: float):
    eigenvalues, eigenvectors = torch.linalg.eigh(M)
    sqrt_eigenvalues = torch.pow(eigenvalues, exponent=exponent)
    sqrt_matrix = eigenvectors @ torch.diag(sqrt_eigenvalues) @ eigenvectors.t()
    return sqrt_matrix


def _estimate_cov_matrix(Sourcemapping: list, SourceEmb: torch.tensor, prop):
    d = SourceEmb.shape[1]  # dim
    c = SourceEmb.shape[0]  # n_classes

    C = torch.zeros(size=(d, d)).to(SourceEmb.device)

    for i in range(c):
        C += prop[i] * torch.cov(Sourcemapping[i].T)

    return C.cpu()


def M_identity(Sourcemapping: list, SourceEmb: torch.tensor, *args, **kwargs):
    d = SourceEmb.shape[1]  # dim
    return torch.eye(d)


def M_estimation(
    Sourcemapping: list,
    SourceEmb: torch.tensor,
    lbda: float = 0.01,
    prop: np.ndarray = None,
    *args,
    **kwargs
):
    SIGMA = _estimate_cov_matrix(Sourcemapping, SourceEmb, prop)
    I = torch.eye(SourceEmb.shape[1]).to(SourceEmb.device)

    return _power_matrix(SIGMA.to(SourceEmb.device) + lbda * I, exponent=-0.5).to(
        device=SourceEmb.device, dtype=torch.float32
    )

