from setuptools import setup, find_packages

# Project metadata
NAME = 'LeQua2024'
DESCRIPTION = 'package for LeQua2024'
VERSION = '0.0'
AUTHOR = 'Bastien Dussap'


setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    author=AUTHOR,
    packages=find_packages(),
    entry_points={
    },
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.10',
    ],
)