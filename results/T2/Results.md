# Results

#### Task 2

|  Methods/Error  |      MRAE      |  MAE |
|:----------|:-------------:|:------:|
| Dummy | 4.84128 ~ 5.16758 | 0.04285 ~ 0.00420 |
| CC | 3.33052 ~ 2.81888 | 0.03397 ~ 0.00493 |
| BBSE |  1.83354 ~ 1.59928 | 0.02022 ~ 0.00481 |
| MLLE |  1.32987 ~ 1.06600 | 0.01513 ~ 0.00385 |
| MeanDFM |  2.22344 ~ 1.58660  | 0.02093 ~ 0.00443 |
| RFFM |  2.28737 ~ 1.58794    | 0.02206 ~ 0.00451 |
| MahalanobisRFFM | 2.27223 ~ 1.57562    | 0.02163 ~ 0.00460 |
| FourierClassifier | 1.73865 ~ 1.30379    | 0.01725 ~ 0.00404 |
| MahalanobisFourierClassifier | 1.61629 ~ 1.21882 | 0.01570 ~ 0.00382 |

# log

### Dummy

2024-02-16 18:12:35,506 - INFO - Running method for T2 task  
2024-02-16 18:12:35,506 - INFO - Method dummy not recognized ; Use dummy method  
2024-02-16 18:12:54,670 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T2/dummy.csv  
2024-02-16 18:12:56,404 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T2/dummy.csv  
2024-02-16 18:12:56,410 - INFO - Format check: [passed]  
2024-02-16 18:12:58,155 - INFO - Running evaluation for T2 task, method dummy  
2024-02-16 18:12:58,235 - INFO - MRAE: 4.84128 ~ 5.16758  
2024-02-16 18:12:58,235 - INFO - MAE: 0.04285 ~ 0.00420  

### CC

2024-02-16 18:10:56,253 - INFO - Running method for T2 task  
2024-02-16 18:10:56,253 - INFO - Running method CC  
2024-02-16 18:11:50,194 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T2/CC.csv 
2024-02-16 18:11:51,900 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T2/CC.csv  
2024-02-16 18:11:51,905 - INFO - Format check: [passed]  
2024-02-16 18:11:53,524 - INFO - Running evaluation for T2 task, method CC  
2024-02-16 18:11:53,604 - INFO - MRAE: 3.33052 ~ 2.81888  
2024-02-16 18:11:53,604 - INFO - MAE: 0.03397 ~ 0.00493 

### BBSE

2024-02-16 18:32:15,451 - INFO - Running method for T1 task  
2024-02-16 18:32:15,451 - INFO - Running method BBSE  
2024-02-16 18:33:24,058 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T2/BBSE.csv  
2024-02-16 18:33:25,895 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T2/BBSE.csv  
2024-02-16 18:33:25,903 - INFO - Format check: [passed]  
2024-02-16 18:33:27,574 - INFO - Running evaluation for T2 task, method BBSE  
2024-02-16 18:33:27,653 - INFO - MRAE: 1.83354 ~ 1.59928  
2024-02-16 18:33:27,653 - INFO - MAE: 0.02022 ~ 0.00481  

### MLLE

2024-02-16 18:33:33,841 - INFO - Running method for T1 task  
2024-02-16 18:33:33,841 - INFO - Running method MLLE  
2024-02-16 18:34:34,964 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T2/MLLE.csv  
2024-02-16 18:34:36,825 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T2/MLLE.csv  
2024-02-16 18:34:36,833 - INFO - Format check: [passed]  
2024-02-16 18:34:38,754 - INFO - Running evaluation for T2 task, method MLLE  
2024-02-16 18:34:38,837 - INFO - MRAE: 1.32987 ~ 1.06600  
2024-02-16 18:34:38,837 - INFO - MAE: 0.01513 ~ 0.00385  


### MeanDFM

2024-02-16 18:34:59,175 - INFO - Running method for T1 task  
2024-02-16 18:34:59,176 - INFO - Running method MeanDFM  
2024-02-16 18:35:25,073 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T2/MeanDFM.csv  
2024-02-16 18:35:26,943 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T2/MeanDFM.csv  
2024-02-16 18:35:26,951 - INFO - Format check: [passed]  
2024-02-16 18:35:28,620 - INFO - Running evaluation for T2 task, method MeanDFM  
2024-02-16 18:35:28,698 - INFO - MRAE: 2.22344 ~ 1.58660  
2024-02-16 18:35:28,699 - INFO - MAE: 0.02093 ~ 0.00443  

### RFFM

2024-02-19 11:33:18,226 - INFO - Running method for T1 task  
2024-02-19 11:33:18,226 - INFO - Running method RFFM 
2024-02-19 11:33:29,929 - INFO - Best sigma: 1.905263157894737  
2024-02-19 11:34:05,873 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T2/RFFM.csv  
2024-02-19 11:34:07,713 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T2/RFFM.csv  
2024-02-19 11:34:07,722 - INFO - Format check: [passed]  
2024-02-19 11:34:09,568 - INFO - Running evaluation for T2 task, method RFFM  
2024-02-19 11:34:09,651 - INFO - MRAE: 2.28737 ~ 1.58794  
2024-02-19 11:34:09,651 - INFO - MAE: 0.02206 ~ 0.00451  

### FourierClassifier

2024-02-19 11:37:42,258 - INFO - Running method for T1 task  
2024-02-19 11:37:42,258 - INFO - Running method FourierClassifier  
2024-02-19 11:39:33,452 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T2/FourierClassifier.csv  
2024-02-19 11:39:35,281 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T2/FourierClassifier.csv  
2024-02-19 11:39:35,289 - INFO - Format check: [passed]   
2024-02-19 11:39:36,970 - INFO - Running evaluation for T2 task, method FourierClassifier   
2024-02-19 11:39:37,050 - INFO - MRAE: 1.73865 ~ 1.30379  
2024-02-19 11:39:37,050 - INFO - MAE: 0.01725 ~ 0.00404  

### MahalanobisFourierClassifier

2024-02-19 14:31:33,299 - INFO - Running method for T1 task  
2024-02-19 14:31:33,299 - INFO - Running method MahalanobisFourierClassifier  
2024-02-19 14:49:44,764 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T2/MahalanobisFourierClassifier.csv  
2024-02-19 14:49:46,571 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T2/MahalanobisFourierClassifier.csv  
2024-02-19 14:49:46,580 - INFO - Format check: [passed]  
2024-02-19 14:49:48,288 - INFO - Running evaluation for T2 task, method MahalanobisFourierClassifier  
2024-02-19 14:49:48,368 - INFO - MRAE: 1.61629 ~ 1.21882  
2024-02-19 14:49:48,368 - INFO - MAE: 0.01570 ~ 0.00382  


### MahalanobisRFFM

2024-02-19 13:48:06,599 - INFO - Running method for T1 task  
2024-02-19 13:48:06,599 - INFO - Running method MahalanobisRFFM  
2024-02-19 13:48:18,089 - INFO - Best sigma: 1.905263157894737  
2024-02-19 13:48:18,089 - INFO - Best sigma: 1.905263157894737  
2024-02-19 13:53:48,968 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T2/MahalanobisRFFM.csv  
2024-02-19 13:53:50,761 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T2/MahalanobisRFFM.csv  
2024-02-19 13:53:50,769 - INFO - Format check: [passed]   
2024-02-19 13:53:52,452 - INFO - Running evaluation for T2 task, method MahalanobisRFFM  
2024-02-19 13:53:52,534 - INFO - MRAE: 2.27223 ~ 1.57562  
2024-02-19 13:53:52,534 - INFO - MAE: 0.02163 ~ 0.00460  
