# Results on Task 1

|  Methods/Error  |      MRAE      |  MAE |
|:----------|:-------------:|:------:|
| Dummy |  3.08663 ~ 13.60694  | 0.34030 ~ 0.22587 |
| CC |  1.32047 ~ 6.05854  | 0.16249 ~ 0.10693 |
| BBSE | 0.17305 ~ 0.62915 | 0.02814 ~ 0.02238 |
| MLLE | 0.11150 ~ 0.32965 | 0.02223 ~ 0.01720 |
| MeanDFM |  0.17266 ~ 0.45374  | 0.03403 ~ 0.02597 |
| RFFM | 0.17907 ~ 0.78369 |  0.03058 ~ 0.02307  |
| MahalanobisRFFM | 0.17094 ~ 0.72385  | 0.02681 ~ 0.02053 |
| FourierClassifier | 0.15437 ~ 0.54801 |  0.02518 ~ 0.02019 |
| MahalanobisFourierClassifier | 0.13678 ~ 0.47734 |  0.02264 ~ 0.01748 |


# log

### Dummy

2024-02-16 17:52:57,820 - INFO - Running method for T1 task  
2024-02-16 17:52:57,820 - INFO - Method dummy not recognized ; Use dummy method  
2024-02-16 17:53:04,740 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T1/dummy.csv  
2024-02-16 17:53:06,418 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T1/dummy.csv  
2024-02-16 17:53:06,420 - INFO - Format check: [passed]  
2024-02-16 17:53:08,058 - INFO - Running evaluation for T1 task, method dummy  
2024-02-16 17:53:08,136 - INFO - MRAE: 3.08663 ~ 13.60694  
2024-02-16 17:53:08,136 - INFO - MAE: 0.34030 ~ 0.22587  

### CC

2024-02-16 18:04:57,011 - INFO - Running method for T1 task  
2024-02-16 18:04:57,011 - INFO - Running method CC  
2024-02-16 18:05:12,129 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T1/CC.csv  
2024-02-16 18:05:13,909 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T1/CC.csv  
2024-02-16 18:05:13,911 - INFO - Format check: [passed]  
2024-02-16 18:05:15,586 - INFO - Running evaluation for T1 task, method CC  
2024-02-16 18:05:15,669 - INFO - MRAE: 1.32047 ~ 6.05854  
2024-02-16 18:05:15,669 - INFO - MAE: 0.16249 ~ 0.10693  

### MLLE

2024-02-16 18:29:22,039 - INFO - Running method for T1 task  
2024-02-16 18:29:22,039 - INFO - Running method MLLE  
2024-02-16 18:29:37,976 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T1/MLLE.csv   
2024-02-16 18:29:39,820 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T1/MLLE.csv  
2024-02-16 18:29:39,822 - INFO - Format check: [passed]  
2024-02-16 18:29:41,547 - INFO - Running evaluation for T1 task, method MLLE  
2024-02-16 18:29:41,628 - INFO - MRAE: 0.11150 ~ 0.32965  
2024-02-16 18:29:41,628 - INFO - MAE: 0.02223 ~ 0.01720  

### BBSE

2024-02-16 18:30:30,177 - INFO - Running method for T1 task  
2024-02-16 18:30:30,177 - INFO - Running method BBSE  
2024-02-16 18:30:46,866 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T1/BBSE.csv  
2024-02-16 18:30:48,669 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T1/BBSE.csv  
2024-02-16 18:30:48,671 - INFO - Format check: [passed]  
2024-02-16 18:30:50,346 - INFO - Running evaluation for T1 task, method BBSE  
2024-02-16 18:30:50,426 - INFO - MRAE: 0.17305 ~ 0.62915  
2024-02-16 18:30:50,426 - INFO - MAE: 0.02814 ~ 0.02238  


### MeanDFM

2024-02-16 18:28:18,304 - INFO - Running method for T1 task  
2024-02-16 18:28:18,304 - INFO - Running method MeanDFM  
2024-02-16 18:28:26,195 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T1/MeanDFM.csv  
2024-02-16 18:28:28,020 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T1/MeanDFM.csv  
2024-02-16 18:28:28,022 - INFO - Format check: [passed]  
2024-02-16 18:28:29,829 - INFO - Running evaluation for T1 task, method MeanDFM  
2024-02-16 18:28:29,908 - INFO - MRAE: 0.17266 ~ 0.45374  
2024-02-16 18:28:29,909 - INFO - MAE: 0.03403 ~ 0.02597  

### RFFM

2024-02-19 11:31:02,570 - INFO - Running method for T1 task  
2024-02-19 11:31:02,570 - INFO - Running method RFFM  
2024-02-19 11:31:06,424 - INFO - Best sigma: 2.678947368421053  
2024-02-19 11:31:18,277 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T1/RFFM.csv  
2024-02-19 11:31:20,034 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T1/RFFM.csv  
2024-02-19 11:31:20,040 - INFO - Format check: [passed]  
2024-02-19 11:31:21,707 - INFO - Running evaluation for T1 task, method RFFM  
2024-02-19 11:31:21,786 - INFO - MRAE: 0.17907 ~ 0.78369  
2024-02-19 11:31:21,786 - INFO - MAE: 0.03058 ~ 0.02307  

### FourierClassifier

2024-02-19 13:17:43,139 - INFO - Running method for T1 task
2024-02-19 13:17:43,139 - INFO - Running method FourierClassifier
2024-02-19 13:18:44,727 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T1/FourierClassifier.csv
2024-02-19 13:18:46,603 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T1/FourierClassifier.csv
2024-02-19 13:18:46,606 - INFO - Format check: [passed]
2024-02-19 13:18:48,417 - INFO - Running evaluation for T1 task, method FourierClassifier
2024-02-19 13:18:48,503 - INFO - MRAE: 0.15437 ~ 0.54801
2024-02-19 13:18:48,503 - INFO - MAE: 0.02518 ~ 0.02019

### MahalanobisFourierClassifier

2024-02-19 14:23:12,240 - INFO - Running method for T1 task
2024-02-19 14:23:12,240 - INFO - Running method MahalanobisFourierClassifier
2024-02-19 14:27:11,007 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T1/MahalanobisFourierClassifier.csv
2024-02-19 14:27:12,829 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T1/MahalanobisFourierClassifier.csv
2024-02-19 14:27:12,831 - INFO - Format check: [passed]
2024-02-19 14:27:14,502 - INFO - Running evaluation for T1 task, method MahalanobisFourierClassifier
2024-02-19 14:27:14,583 - INFO - MRAE: 0.13678 ~ 0.47734
2024-02-19 14:27:14,583 - INFO - MAE: 0.02264 ~ 0.01748

### MahalanobisRFFM

2024-02-19 13:38:07,480 - INFO - Running method for T1 task  
2024-02-19 13:38:07,480 - INFO - Running method MahalanobisRFFM  
2024-02-19 13:38:11,236 - INFO - Best sigma: 2.678947368421053  
2024-02-19 13:38:11,236 - INFO - Best sigma: 2.678947368421053  
2024-02-19 13:39:51,114 - INFO - Results dumped to /home/dussap/Documents/These/Projet/LeQua2024/results/T1/MahalanobisRFFM.csv  
2024-02-19 13:39:52,861 - INFO - Running format-checker for /home/dussap/Documents/These/Projet/LeQua2024/results/T1/MahalanobisRFFM.csv  
2024-02-19 13:39:52,863 - INFO - Format check: [passed]  
2024-02-19 13:39:54,516 - INFO - Running evaluation for T1 task, method MahalanobisRFFM  
2024-02-19 13:39:54,597 - INFO - MRAE: 0.17094 ~ 0.72385  
2024-02-19 13:39:54,597 - INFO - MAE: 0.02681 ~ 0.02053  

