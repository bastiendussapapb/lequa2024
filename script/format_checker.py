import argparse, os, logging
from lequa2024.data import ResultSubmission
from lequa2024.constants import LEQUA2024_TASKS


"""
LeQua2024 Official format-checker script 
"""

def main(prevalence_file, logger):
    logger.info(f"Running format-checker for {prevalence_file}")
    try:
        ResultSubmission.check_file_format(prevalence_file)
    except Exception as e:
        print(e)
        logger.error('Format check: [not passed]')
    else:
        logger.info('Format check: [passed]')


if __name__=='__main__':
    # Parser
    parser = argparse.ArgumentParser(description='LeQua2024 format-checker')
    parser.add_argument('--method', 
                        type=str,
                        help='Name of the method')
    parser.add_argument('--task', metavar='TASK', type=str, choices=LEQUA2024_TASKS,
                        help='Task name (T1, T2, T3, T4)')
    
    args = parser.parse_args()
    prevalence_file = os.path.join(os.getcwd(), f"results/{args.task}/{args.method}.csv")
    assert os.path.exists(prevalence_file), f"File {prevalence_file} does not exist"

    # Set up logging
    logging.basicConfig(
        filename=os.path.join(os.getcwd(), f"log/{args.task}.log"),
        filemode="a",
        level=logging.INFO,
        format="%(asctime)s - %(levelname)s - %(message)s",
    )
    logger = logging.getLogger(__name__)

    main(prevalence_file, logger=logger)
