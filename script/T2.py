""" 
Script for T2 of lequa2024
"""

from typing import Generator
import argparse
import logging
import os
import lequa2024 as lq

from quapy.data import LabelledCollection
from collections import defaultdict


def main(
    Source: LabelledCollection,
    Target_generator: Generator,
    results_path: str,
    logger: logging.Logger,
    method: str,
    seed: float,
):
    logger.info(f"Running method for T1 task")
    dump_path = os.path.join(results_path, f"{method}.csv")

    if method == "CC":
        logger.info(f"Running method {method}")
        results = lq.methods.ClassifyAndCount(
            Source, Target_generator, seed)
        
    elif method == "BBSE":
        logger.info(f"Running method {method}")
        results = lq.methods.BlackBoxShiftEstimator(
            Source, Target_generator, seed)

    elif method == "MLLE":
        logger.info(f"Running method {method}")
        results = lq.methods.MaximumLikelihoodShiftEstimator(
            Source, Target_generator, seed)
        
    elif method == "MeanDFM":    
        logger.info(f"Running method {method}")
        results = lq.methods.MeanDFM(Source, Target_generator, seed)
        
    elif method == "FourierClassifier":
        logger.info(f"Running method {method}")
        results = lq.methods.FourierClassifier(Source, Target_generator, seed)
        
    elif method == "MahalanobisFourierClassifier":
        logger.info(f"Running method {method}")
        results = lq.methods.MahalanobisFourierClassifier(Source, Target_generator, seed)
        
    elif method == "RFFM":
        logger.info(f"Running method {method}")
        results = lq.methods.RandomFourierFeatureMatching(Source, Target_generator, seed, logger)
        
    elif method == "RFFMPCA":
        logger.info(f"Running method {method}")
        results = lq.methods.RandomFourierFeatureMatchingPCA(Source, Target_generator, seed, logger)
    
    elif method == "MahalanobisRFFM":
        logger.info(f"Running method {method}")
        results = lq.methods.MahalanobisRandomFourierFeatureMatching(Source, Target_generator, seed, logger)
        
    else:
        logger.info(f"Method {method} not recognized ; Use dummy method")
        results = lq.methods.dummy_method(Source, Target_generator)

    results.dump(dump_path)
    logger.info(f"Results dumped to {dump_path}")


if __name__ == "__main__":
    # Parse arguments
    parser = argparse.ArgumentParser(description="LeQua2024 T2 task")
    parser.add_argument(
        "--method",
        type=str,
        help="Method to use for T2 task",
        required=True,
    )
    args = parser.parse_args()

    # Set up logging
    logging.basicConfig(
        filename=os.path.join(os.getcwd(), "log/T2.log"),
        filemode="w",
        level=logging.INFO,
        format="%(asctime)s - %(levelname)s - %(message)s",
    )
    logger = logging.getLogger(__name__)

    # Set up results path
    results_path = os.path.join(os.getcwd(), "results/T2")

    # Set up data path
    source_path = os.path.join(
        os.getcwd(), "data/T2.train_dev/T2/public/training_data.txt"
    )
    assert os.path.exists(source_path), f"Path {source_path} does not exist"

    target_dir_path = os.path.join(
        os.getcwd(), "data/T2.train_dev/T2/public/dev_samples"
    )
    assert os.path.exists(
        target_dir_path), f"Path {target_dir_path} does not exist"

    # Set up Source/Target
    Source = lq.preprocess.create_LabelledCollection_from_documents(
        path=source_path)
    Target_generator = lq.data.gen_load_samples(
        path_dir=target_dir_path, ground_truth_path=None, return_id=True
    )

    # Seed
    entropy = 0x87

    main(Source=Source,
         Target_generator=Target_generator,
         results_path=results_path,
         logger=logger,
         method=args.method,
         seed=entropy)
